<?php
/**
 * 统一入口
 */
define('API_BING', TRUE);
define('D_S', DIRECTORY_SEPARATOR);
require_once 'init.php';

DI()->loader->addDirs(array('.',DI()->config->get('app.module_root_dir'))); //挂载此目录是为了实现自动加载Core的类

/* 定义模块常量 */
define('MODULE_ROOT_NAME',DI()->config->get('app.module_root_dir')); //模块根目录名称
define('MODULE_ROOT_PATH',API_ROOT.D_S.MODULE_ROOT_NAME); //模块根目录路径
/* 加载核心函数 */
require_once API_ROOT . D_S.'Sima'.D_S.'functions.php';

/* 默认拦截器 */
//DI()->filter = 'PhalApi_Filter_SimpleMD5'; //签名验证服务，默认使用PhalApi提供的MD5拦截器
DI()->UserFilter='Common_Filter_User'; //登录状态验证
DI()->AuthFilter='Common_Filter_Auth'; //权限验证

$api = new Sima_Sima();
$rs = $api->response();
$rs->output();

