<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(

    /**
     * 框架配置
     */
    'version'=>'1.0',                   //框架版本
    'module_root_dir'=>"Apps",                 //模块根目录

    /**
     * 应用接口层的统一参数
     */
    'apiCommonRules' => array(
        //'sign' => array('name' => 'sign', 'require' => true),
    ),


);
