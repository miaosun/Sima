<?php
/**
 * 模块信息
 */

return array(
	'param'=>array(
		'name' => '系统管理',
        'description' => '核心模块。提供系统管理功能',
        'author' => 'miaosun @ sima Team Inc',
        'version' => '1.0',
	),
);