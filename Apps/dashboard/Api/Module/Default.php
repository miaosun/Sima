<?php

/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */
class Api_Module_Default extends PhalApi_Api
{

    public function getRules()
    {
        return array(
            'index' => array(
                'username' => array('name' => 'username', 'default' => 'PHPer',),
            ),
        );
    }

    /**
     * 默认接口服务
     * @return string title 标题
     * @return string content 内容
     * @return string version 版本，格式：X.X.X
     * @return int time 当前时间戳
     */
    public function index()
    {
        common_load('demo2@functions');
        return array('info' => '这是 ' . MODULE_NAME . ' 模块的 Module_Default.index 接口', 'func' => say_hello(),'lang'=>T('module_lang'));
    }

    public function index2()
    {
        return array('info' => '这是 ' . MODULE_NAME . ' 模块的 index2 接口', 'func' => say());
    }
}
