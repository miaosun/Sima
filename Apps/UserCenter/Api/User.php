<?php
/**
 * Created by PhpStorm.
 * User: miaos
 * Date: 2015/11/3 0003
 * Time: 10:44
 */
class Api_User extends Sima_BaseApi{

    public function getRules() {
        return array(
            'login'=>array(
                'username' => array('name' => 'username', 'require' => true, 'min' => 1, 'max' => 28, 'desc'=>'用户名'),
                'password' => array('name' => 'password', 'require' => true, 'min' => 1, 'max' => 150, 'desc'=>'密码'),
                'client' => array('name' => 'client', 'require' => false, 'desc'=>'客户端来源'),
            ),

        );
    }


    /**
     * 用户登录
     * @return int $code 业务代码
     * @return array $info[] 业务数据
     * @return int $info[].uid 用户id
     * @return string $info[].token 用户token
     * @return string $msg 业务消息
     */
    public function login(){

        $rs = array('code' => 0, 'info' => array(), 'msg' => '');
        $domain=new UserCenter_Domain_User();
        $uid=$domain->login($this);

        // 用户不存在
        if($uid==0){
            $rs['code']=1;
            $rs['msg']=T('user not');
        }

        // 密码错误
        if($uid==-1){
            $rs['code']=2;
            $rs['msg']=T('pwd error');
        }

        // 创建会话
        if($uid>=1){
            $token = UserCenter_Domain_Session::generate($uid,$this->client);
            $rs['info']['uid'] = $uid;
            $rs['info']['token'] = $token;
        }

        return $rs;
    }


    /**
     * 注销
     */
    public function logout(){
        DI()->userLite->logout();
        $rs = array('code' => 0, 'info' => array(), 'msg' => 'logout Success');
        return $rs;

    }
}