<?php
/**
 * 模块信息
 */

return array(
	'param'=>array(
		'name' => '用户中心',
        'description' => '核心模块。提供用户管理，登录验证等功能',
        'author' => 'miaosun @ sima Team Inc',
        'version' => '1.0',
	),
);