<?php
/**
 * 翻译说明：
 *
 * 1、带大括号的保留原来写法，如：{name}，会被系统动态替换
 * 2、没在以下出现的，可以自行追加
 *
 * @author dogstar <chanzonghuang@gmail.com> 2015-02-09
 */

return array(
    'user not'=>'用户不存在',
    'pwd error'=>'用户密码不正确',
    'user not login'=>'用户没有登录',
    'Session expired'=>'用户会话已过期',
);
