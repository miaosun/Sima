<?php

class UserCenter_Model_User extends PhalApi_Model_NotORM {

	protected function getTableName($id) {
		return 'user';
	}

	public function login($info){
		$username=$info['username'];
		$password=$info['password'];

		$rows =self::getORM()
			->select('*')
			->where('username', $username)
			->fetchOne();

		if($rows===false){
			return 0;
		}

		if($rows['password']!=md5($password)){
			return -1;
		}

		return $rows['id'];
	}
}
