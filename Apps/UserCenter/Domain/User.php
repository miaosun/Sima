<?php
/**
 * Created by PhpStorm.
 * User: miaos
 * Date: 2015/11/4 0004
 * Time: 16:04
 */
class UserCenter_Domain_User {

    public function login($obj){
        $vars = get_object_vars($obj);
        $model=new UserCenter_Model_User();
        $r=$model->login($vars);
        return $r;
    }

}