#- 简介
Module API Framework ，简称MAF，是一个基于PhalApi的模块化Api开发框架，它是为了实现快速搭建一个模块化API开发的环境，主要用于中小型OA、ERP、协同办公等类型的管理系统的多端应用服务端api开发。

####目前框架还在开发中...

#- 主要目录结构
```

|——— Module //模块目录
|       |—— Dome //dome模块
|       |   |—— Api //响应层
|       |   |—— Domain //领域层
|       |   |—— Model //持久层
|       |   |—— Common //模块公共文件（存放模块公共函数或公共类）
|       |   |—— Language //模块语言
|       |   |—— Install //模块安装
|       |   |—— ModelProxy //代理层
|       |—— ...更多模块
|——— Common //框架公共文件
|——— Config //框架配置文件
|——— Core   //框架核心文件
|——— Data   //数据文件
|——— install //框架安装文件
|——— Language //公共语言文件
|——— Library //扩展类
|——— Public //公开访问接口
|       |—— init.php //统一初始化
|       |—— index.php //统一入口
|——— Runtime //运行临时文件

```
#- 访问接口的方式
访问方式更PhalApi有比较大的差别，必须要提供两个必要的url参数，如：
```
http://localhost/public/?m=demo
http://localhost/public/?m=demo&amp;s=Default.index
```
参数说明：<br>
m：指定一个模块<br>
s：指定一个接口（其实这个就是原来service的简写）<br>
错误：<br>
如果没有指定m参数，或者指定的m参数指向的模块不存在，都会抛出一个400的错误<br>（暂时没想好抛出啥错误）。

#- 框架初始化
模块化框架，涉及到内置的模块初始化，所以在没有通过安装向导进行安装的话，<br>在访问接口时，会抛出一个异常
提示“系统未初始化”，要正确使用框架，请进行安装。
### 安装方法：
```
http://localhost/install
```
访问上面的地址，即可进入安装向导，然后通过指引一步一步安装。

#- 模块开发
....

