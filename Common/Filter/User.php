<?php
/**
 * 默认用户登录验证拦截器
 *
 * @author miaosun <miaosun009@126.com> 2015-10-24
 */
class Common_Filter_User implements PhalApi_Filter
{
    public function check()
    {
	    // 免登录验证接口白名单
        $all=DI()->config->get('wlist.login');
        $all=$all=array_map('strtolower',$all);
        $service=strtolower(MODULE_NAME.'@'.DI()->request->get('s'));

        if(!in_array($service,$all)){
            DI()->userLite=new UserCenter_Common_UserLite();
            DI()->userLite->check(true);
        }


    }
}