<?php defined('PHALAPI_INSTALL') || die('no access'); ?>
<?php include dirname(__FILE__) . DIRECTORY_SEPARATOR . '_header.php'; ?>
    <div class="radius bg bouncein window window_small" >
        <div class="window_title t_normal">
            <span class="icon-circle"> </span>
            <span class="icon-circle"></span>
            <span class="margin-small-left">Comb Install</span>
        </div>
        <div class="padding-large text-black">
            <h1 class="margin-small-bottom" >欢迎使用，模块化 API 开发框架</h1>
            <h5 class="margin-big-bottom ">用另一种思维，重新构建你的应用</h5>
            <hr>
            <div class="margin-big-top" >
                <a class="button bg-main margin-small-right"  href=".?op=envCheck" role="button">  同意安装  </a>
                <button class="button">取消</button>
            </div>
        </div>
    </div>
</div>
<?php include dirname(__FILE__) . DIRECTORY_SEPARATOR . '_footer.php'; ?>