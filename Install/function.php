<?php

/**
 * 获取内置模块信息
 * @return array
 */
function getModules(){
    return array(
        'dome'=>'默认模块',
    );
}

/**
 * 获取模块中文名
 * @param $moduleName
 * @return mixed
 */
function getModuleName($moduleName){
    $modules= getModules();
    return $modules[$moduleName];
}

/**
 * 安装模块
 * @param $module
 * @return bool
 */
function install($moduleName){
    //此处实现安装过程，返回true或false
    return true;
}