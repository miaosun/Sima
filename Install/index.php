<?php
/**
 * 安装处理
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
define('PHALAPI_INSTALL', TRUE);
define('D_S', DIRECTORY_SEPARATOR);
include_once dirname(__FILE__) . D_S . 'function.php';

$allowOptions = array('start', 'envCheck', 'dbInit', 'installing', 'installResult');
$option = $_GET['op'];
if (empty($option) || !in_array($option, $allowOptions)) {
    $option = 'start';
}

/* 检测是否已安装 */
if (file_exists('_install.lock')) {
    $error = '框架已安装，若要重新安装，请删除目录下的_install.lock文件';
    $retry = './?op=start';
    include dirname(__FILE__) . D_S . '_error.php';
    exit();
}

if ($option == 'start') { /* 安装启始页 */
    include dirname(__FILE__) . D_S . '_start.php';
    exit();
} elseif ($option == 'envCheck') { /* 环境检测 */

    //-1：必须但不支持 0：可选但不支持 1：完美支持
    $checkList = array(
        'php' => array('name' => 'PHP 版本', 'status' => -1, 'tip' => '建议使用PHP 5.3.3及以上版本，否则DI无法支持匿名函数'),
        'pdo' => array('name' => '数据库模块', 'status' => -1, 'tip' => '建议使用PDO扩展，否则NotORM无法使用PDO进行数据库操作'),
        'memcache' => array('name' => 'Memcache扩展', 'status' => 0, 'tip' => '无此扩展时，不能使用Memcache缓存'),
        'mcrypt' => array('name' => 'Mcrypt扩展', 'status' => 0, 'tip' => '无此扩展时，不能使用mcrypt进行加密处理'),
        'runtime' => array('name' => '目录权限', 'status' => -1, 'tip' => '日志目录若缺少写入权限，则不能写入日记和进行文件缓存'),
    );

    if (version_compare(PHP_VERSION, '5.3.3', '>=')) {
        $checkList['php']['status'] = 1;
    }
    if (class_exists('PDO', false) && extension_loaded('PDO')) {
        $checkList['pdo']['status'] = 1;
    }
    if (class_exists('Memcache', false) && extension_loaded('memcache')) {
        $checkList['memcache']['status'] = 1;
    }
    if (extension_loaded('mcrypt')) {
        $checkList['mcrypt']['status'] = 1;
    }
    $runtimePath = dirname(__FILE__) . implode(D_S, array('', '..', 'Runtime'));
    $runtimePath = file_exists($runtimePath) ? realpath($runtimePath) : $runtimePath;
    $checkList['runtime']['tip'] = $runtimePath . '<br>' . $checkList['runtime']['tip'];
    if (is_writeable($runtimePath)) {
        $checkList['runtime']['status'] = 1;
    }
    include dirname(__FILE__) . D_S . '_envCheck.php';

} elseif ($option == 'dbInit') { /* 配置系统 */

    if (isset($_POST['submitDbInit'])) {
        //数据库配置文件
        $search = array('{host}','{name}','{user}','{password}','{port}','{charset}','{prefix}',);
        $replace = array($_POST['host'],$_POST['name'],$_POST['user'],$_POST['password'],$_POST['port'],$_POST['charset'],$_POST['prefix'],);
       //检测数据库连接情况
        $mysqliConn = new mysqli();
        @$mysqliConn->connect($_POST['host'], $_POST['user'], $_POST['password'], '', $_POST['port']);
        if ($mysqliConn->connect_error) {
            $error = '数据库连接错误：' . $mysqliConn->connect_error;
            $retry = './?op=dbInit';
            include dirname(__FILE__) . D_S . '_error.php';
            exit();
        }

        /*-------------*/
        //此处可以实现数据库和默认数据的创建
        /*-------------*/

        // 判断数据库能否创建
        $dbName=$_POST['name'];
        if ( $mysqliConn->get_server_info() > '4.1' ) {
            $mysqliConn->query( "CREATE DATABASE IF NOT EXISTS `$dbName` DEFAULT CHARACTER SET " . $_POST['charset']);
        } else {
            $mysqliConn->query( "CREATE DATABASE IF NOT EXISTS `$dbName`");
        }

        if ( $mysqliConn->error ) {
            $error = '数据库错误：' . $mysqliConn->error;
            $retry = './?op=dbInit';
            include dirname(__FILE__) . D_S . '_error.php';
            exit();
        }


        //生成数据库配置文件
        $configDbsContent = str_replace($search, $replace, getConfigDbsTpl());
        file_put_contents(
            dirname(__FILE__) . implode(D_S, array('', '..', 'Config', 'dbs.php')),
            $configDbsContent
        );

        $mysqliConn->close();
        header("Location: ./?op=installing");
    } else {
        include dirname(__FILE__) . D_S . '_dbInit.php';
        exit();
    }
} elseif ($option == 'installing') { /* 安装过程 */
    if (isset($_GET['installBegin']) && $_GET['installBegin'] == 1) {
        $installModules = $_POST['installModules']; // 要安装的模块
        $installModules = json_decode($installModules);
        $installingModule = $_POST['installingModule'];
        if (empty($installingModule)) {
            $installingModule = $installModules[0];
        }
        $moduleNums = count($installModules);
        $isSuccess = install($installingModule); // 执行安装模块
        if ($isSuccess) {
            foreach ($installModules as $k => $module) {
                if ($module == $installingModule) {
                    $index = $k + 1;
                    if ($index < count($installModules)) {
                        $nextModule = $installModules[$index]; // 下一个要安装的模块
                        $nextModuleName = getModuleName($nextModule); // 下一个要安装的模块名
                        $process = number_format(($index / $moduleNums) * 100, 1) . '%'; // 完成度
                        echo json_encode(array('complete' => 0, 'isSuccess' => 1, 'process' => $process, 'nextModule' => $nextModule, 'nextModuleName' => $nextModuleName));
                    } else {
                        echo json_encode(array('complete' => 1, 'process' => '100%'));
                    }
                    exit();
                }
            }
        } else {
            echo json_encode(array('complete' => 0, 'isSuccess' => 0, 'msg' => '安装 ' . getModuleName($installingModule) . ' 出错', 'retry' => './?op=installing'));
            exit();
        }

    } else {
        $installModules = array_keys(getModules());
        include dirname(__FILE__) . D_S . '_installing.php';
        exit();
    }

} elseif ($option == 'installResult') { /* 安装结束 */
    if (isset($_GET['res']) && $_GET['res'] == 1) {
        touch('_install.lock');
        include dirname(__FILE__) . D_S . '_complete.php';
    } else {
        $error = $_GET['error'];
        $retry = $_GET['retry'];
        include dirname(__FILE__) . D_S . '_error.php';
    }
}


function getConfigDbsTpl()
{
    $configDbs = <<<EOT
<?php
/**
 * 分库分表的自定义数据库路由配置
 */

return array(
    /**
     * DB数据库服务器集群
     */
    'servers' => array(
        'db_sima' => array(                         //服务器标记
            'host'      => '{host}',             //数据库域名
            'name'      => '{name}',               //数据库名字
            'user'      => '{user}',                  //数据库用户名
            'password'  => '{password}',	                    //数据库密码
            'port'      => '{port}',                  //数据库端口
            'charset'   => '{charset}',                  //数据库字符集
        ),
    ),

    /**
     * 自定义路由表
     */
    'tables' => array(
        //通用路由
        '__default__' => array(
            'prefix' => '{prefix}',
            'key' => 'id',
            'map' => array(
                array('db' => 'db_sima'),
            ),
        ),

        /**
        'System' => array(                                                //表名
            'prefix' => '{prefix}',                                         //表名前缀
            'key' => 'id',                                              //表主键名
            'map' => array(                                             //表路由配置
                array('db' => 'db_sima'),                               //单表配置：array('db' => 服务器标记)
                array('start' => 0, 'end' => 2, 'db' => 'db_db_sima'),     //分表配置：array('start' => 开始下标, 'end' => 结束下标, 'db' => 服务器标记)
            ),
        ),
         */
    ),
);

EOT;

    return $configDbs;
}
