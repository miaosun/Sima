<?php include dirname(__FILE__) . DIRECTORY_SEPARATOR . '_header.php'; ?>
<div class="radius bg bouncein window window_small">
    <div class="window_title t_normal">
        <span class="icon-circle"> </span>
        <span class="icon-circle"></span>
        <span class="margin-small-left">安装中...</span>
    </div>
    <div class="padding-large text-black">
        <h1 class="margin-small-bottom" >程序安装中</h1>
        <h4 class="margin-big-bottom">后台处理数据中，请不要关闭该页面。</h4>
        <div class="progress progress-striped active"><span>0%</span> <div class="progress-bar bg-main" id="progressbar" style="width:0%;"></div> </div>
        <div class="margin-top" >
            <p class="text-small" id="install_info"></p>
        </div>
    </div>
</div>

<?php include dirname(__FILE__) . DIRECTORY_SEPARATOR . '_footer.php'; ?>
<script>
    $(document).ready(function(){

        var installModules = '<?php echo json_encode($installModules) ?>'
        var installModulesObj = JSON.parse(installModules)
        var installUrl = ".?op=installing&installBegin=1"

         //安装模块方法
        function install(module) {
            $.post(installUrl, {installModules: installModules, installingModule: module}, function(res) {
                if (res.complete) {
                    $("#progressbar").css("width", res.process);
                    $("#show_process").text(res.process);
                    $("#install_info").text("安装完成");
                    window.location.href = "./?op=installResult&res=1";
                } else {
                    if (res.isSuccess) {
                        $("#progressbar").css("width", res.process);
                        $("#install_info").text('正在安装：'+res.nextModuleName);
                        install(res.nextModule);
                    } else {
                        $("#install_info").html('<span style="color: red">'+res.msg+"</span>");
                        window.location.href = "./?op=installResult&res=0&error="+res.msg+'&retry='+res.retry;
                    }
                }
            }, 'json');
        }
        // 初始化页面开始安装模块
        var firstModuleName = "<?php echo getModuleName( $installModules['0'] ); ?>";
        $("#install_info").text('正在安装：'+firstModuleName);
        install(installModulesObj[0]);

    })
</script>
