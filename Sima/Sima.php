<?php
/**
 * Comb应用类
 *
 * - 实现远程服务的响应、调用等操作
 *
 * @author miaosun <miaosun009@126.com> 2015.10.23
 */

class  Sima_Sima {

    /**
     * 响应操作
     *
     * 通过工厂方法创建合适的控制器，然后调用指定的方法，最后返回格式化的数据。
     *
     * @return mixed 根据配置的或者手动设置的返回格式，将结果返回
     *  其结果包含以下元素：
    ```
     *  array(
     *      'ret'   => 200,	            //服务器响应状态
     *      'data'  => array(),	        //正常并成功响应后，返回给客户端的数据
     *      'msg'   => '',		        //错误提示信息
     *  );
    ```
     */
    public function response() {
        $data = array_change_key_case($_REQUEST, CASE_LOWER);
        if (isset($data['s'])) {
            $data['service'] = $data['s'];
        }
        DI()->request=new PhalApi_Request($data);
        $rs = DI()->response;
        try {
            $api = Sima_Factory::generateService();
            $service = DI()->request->get('service', 'Default.Index');
            list($apiClassName, $action) = explode('.', $service);
            $rs->setData(call_user_func(array($api, $action)));
        } catch (PhalApi_Exception $ex) {
            $rs->setRet($ex->getCode());
            $rs->setMsg($ex->getMessage());
        } catch (Exception $ex) {
            throw $ex;
        }

        return $rs;
    }



}