<?php
/**
 * 项目公共函数库
 *
 * @author: miaosun <409859374@qq.com> 2015-10-16
 */


/**
 * 加载常用文件（函数或类文件）
 * 参数语法：模块@路径_文件名
 * 例子：
 * admin@common.func ：加载admin模块的common.fun文件
 * admin@func_common.func ：加载admin模块常用文件夹中的func目录下的common.fun文件
 * @common.func ：加载当前模块常用文件夹中的common.func 文件
 * common.func ：加载项目常用文件夹中的common.func文件
 *
 * @param $fileName string 文件名
 */

function common_load($fileName){

    $exp=explode('@',$fileName,2);
    if(count($exp)>1){
        if($exp[0]==""){
            //当前模块加载文件
            require_once MODULE_ROOT_PATH.D_S.MODULE_NAME.D_S.'Common'.D_S.str_replace('_',D_S ,$exp[1]).'.php';
        }else{
            //指定模块加载文件
            require_once MODULE_ROOT_PATH.D_S.$exp[0].D_S.'Common'.D_S.str_replace('_',D_S ,$exp[1]).'.php';
        }
    }else{
        //项目目录加载文件
        require_once API_ROOT.D_S.'Common'.D_S.str_replace('_',D_S ,$fileName).'.php';
    }
}


