<?php
/**
 * comb 创建控制器类 工厂方法
 *
 * 将创建与使用分离，简化客户调用，负责控制器复杂的创建过程
 */

class Sima_Factory {

	/**
     * 创建服务器
     * 根据客户端提供的接口服务名称和需要调用的方法进行创建工作，如果创建失败，则抛出相应的自定义异常
     *
     * 创建过程主要如下：
     * - 1、 系统是否安装
     * - 2、 是否缺少模块
     * - 3、 是否缺少控制器名称和需要调用的方法
     * - 4、 控制器文件是否存在，并且控制器是否存在
     * - 5、 方法是否可调用
     * - 6、 控制器是否初始化成功
     * - 7、 加载模块默认函数
     * - 8、 加载模块默认语言
     *
     * @param boolen $isInitialize 是否在创建后进行初始化
     * @param string $_REQUEST['service'] 接口服务名称，格式：XXX.XXX
     * @return PhalApi_Api 自定义的控制器
     *
     * @uses PhalApi_Api::init()
     * @throws PhalApi_Exception_BadRequest 非法请求下返回400
     */
	static function generateService($isInitialize = TRUE) {
        self::check_install();
        /* 模块分流 */
        $module=DI()->request->get('m',null);
        if(empty($module)){
            throw new PhalApi_Exception_BadRequest(T('请指定一个模块'));
        }
        if(file_exists(MODULE_ROOT_PATH.D_S.$module)==false){
            throw new PhalApi_Exception_BadRequest($module.T('模块不存在'));
        }
        DI()->loader->addDirs(MODULE_ROOT_NAME.D_S.$module);
        define('MODULE_NAME',$module);
        self::load_default_func();
        self::load_module_language();
        $api=PhalApi_ApiFactory::generateService();
		return $api;
	}

    /**检查MAF是否安装，没有安装抛出异常
     * @throws PhalApi_Exception
     */
    private static function check_install(){
        if(!file_exists(API_ROOT.D_S.'install'.D_S.'_install.lock')){
            throw new PhalApi_Exception_InternalServerError(T('MAF is not install'),1);
        }
    }

    /**
     * 加载默认函数库
     */
    private static function load_default_func(){
        $PublicPath=API_ROOT.D_S.'Common'.D_S.'functions.php';
        if(file_exists($PublicPath)){
            require_once $PublicPath;
        }
        $ModulePath=MODULE_ROOT_PATH.D_S.MODULE_NAME.D_S.'Common'.D_S.'functions.php';
        if(file_exists($ModulePath)){
            require_once $ModulePath;
        }
    }

    /**
     * 加载模块语言文件
     */
    private static function load_module_language(){
        PhalApi_Translator::addMessage(MODULE_ROOT_PATH.D_S.MODULE_NAME);
    }
	
}
