<?php
/**
 * Comb Api 基类
 *
 * 所有模块的API层继承该基类
 *
 *  * @author miaosun <miaosun009@126.com> 2016-10-23
 */
class Sima_BaseApi extends PhalApi_Api {


    public function init(){
        parent::init();
        $this->authCheck();
    }

    /**
     * 用户登录检测
     */
    protected function userCheck() {
        $filter=DI()->get('UserFilter','PhalApi_Filter_None');
        self::filterExecute($filter);
    }

    /**
     * 权限验证
     */
    protected function authCheck(){
        $filter=DI()->get('AuthFilter','PhalApi_Filter_None');
        self::filterExecute($filter);
    }

    protected function filterExecute($filter){
        if (isset($filter)) {
            if (!($filter instanceof PhalApi_Filter)) {
                throw new PhalApi_Exception_InternalServerError(
                    T('should be instanceof PhalApi_Filter'));
            }
            $filter->check();
        }
    }

}